﻿# Мод пилится на базе нетленки от АБЦБ - его сюжет и подача мне куда симпатичнее оригинальной стори.
# За что ему огромный респектище и, по возможности, оставлены отсылки на оригинальные правки.
init -1:
    $ alt_release_no = "0.25.c"
    $ alt_compatible_release_no = ["0.00.x", "0.25.c"]

init python:
    mods["scenario__alt__sevendl"] = u"7 Дней Лета"
    mod_tags["scenario__alt__sevendl"] = ["length:days","gameplay:vn","protagonist:male"]

label alt_init:
    call alt_init_names
    call alt_init_vars
    call alt_init_maps
# только если игру начали заново - принимаем номер релиза сохранения по номеру релиза мода
    $ alt_save_release_no = alt_release_no
    return

label scenario__alt__sevendl:
    call alt_init
    jump alt_day0_prologue

